""" A simple timing function that can be used as decorator """
import time


def timeit(method):
    """ return runtime in seconds of a function """
    def timed(*args, **kw):
        tstart = time.time()
        result = method(*args, **kw)
        tend = time.time()
        print('Time of {} (s): {}'.format(method.__name__, tend-tstart))
        return result
    return timed
