""" standard map() method to apply a func to multiple data """
import itertools
from sympy import isprime
from fib2 import fibonacci
from timing import timeit


@timeit
def sequential_map():
    """ find the prime numbers from the first 5000 fibonacci numbers """
    fib_chunk_size = 5000
    fchunk = list(itertools.islice(fibonacci(), fib_chunk_size))
    prmask = map(isprime, fchunk)
    primes = list(itertools.compress(fchunk, prmask))
    print('number of fibonacci numbers', fib_chunk_size)
    print('number of primes', len(primes))


if __name__ == '__main__':
    sequential_map()
