""" start with: mpiexec -n 5 python -m mpi4py mpi_comm_executor.py """
import itertools
from mpi4py.futures import MPICommExecutor
from sympy import isprime
from fib2 import fibonacci
from timing import timeit


@timeit
def mpi_comm_map():
    """ find the prime numbers from the first 5000 fibonacci numbers """
    fib_chunk_size = 5000
    chunk_size_per_proc = 256

    fchunk = list(itertools.islice(fibonacci(), fib_chunk_size))
    with MPICommExecutor() as pool:
        prmask = pool.map(isprime, fchunk, chunksize=chunk_size_per_proc)
        primes = list(itertools.compress(fchunk, prmask))
        print('number of fibonacci numbers', fib_chunk_size)
        print('chunk size per proc', chunk_size_per_proc)
        print('number of primes', len(primes))


if __name__ == '__main__':
    mpi_comm_map()
