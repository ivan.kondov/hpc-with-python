""" use the concurrent.futures package to parallelize the map function """
import itertools
from concurrent.futures import ProcessPoolExecutor
from sympy import isprime
from fib2 import fibonacci
from timing import timeit


@timeit
def futures_pool_map():
    """ find the prime numbers from the first 5000 fibonacci numbers """
    procs = 4
    fib_chunk_size = 5000
    chunk_size_per_proc = 256

    fchunk = list(itertools.islice(fibonacci(), fib_chunk_size))
    with ProcessPoolExecutor(procs) as executor:
        prmask = executor.map(isprime, fchunk, chunksize=chunk_size_per_proc)
        primes = list(itertools.compress(fchunk, prmask))

    print('procs', procs)
    print('number of fibonacci numbers', fib_chunk_size)
    print('chunk size per proc', chunk_size_per_proc)
    print('number of primes', len(primes))


if __name__ == '__main__':
    futures_pool_map()
