""" demonstrate the deadlock in case of exception """
from mpi4py import MPI

comm = MPI.COMM_WORLD
assert comm.Get_size() == 2
rank = comm.Get_rank()
if rank == 0:
    assert 0 == 1
    comm.send({}, dest=1, tag=1)
elif rank == 1:
    # assert 0 == 1
    dct = comm.recv(source=0, tag=1)
