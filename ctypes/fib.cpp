/* C++ class for generating the Fibonacci numbers
   compile with: g++ -c -fPIC fib.cpp -o fib.o */

class Fibonacci {
    int a, b;
    public:
        Fibonacci() {
            a = 0;
            b = 1;
        }
        int next() {
            int ret = a;
            int bnew = a + b;
            a = b;
            b = bnew;
            return ret;
        }
};
