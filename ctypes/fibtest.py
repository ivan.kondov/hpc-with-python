""" a test script for the python extension to the fibonacci C++ library """
import itertools
import fib

fibobj = fib.Fibonacci()
print(list(itertools.islice(fibobj, 10)))
