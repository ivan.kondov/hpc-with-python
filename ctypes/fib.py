""" extension module which imports a DLL compiled with
    g++ -shared -Wl,-soname,fibc++ -o fibc++.so fib.o fib_ext.o """
from ctypes import cdll


lib = cdll.LoadLibrary('./fibc++.so')


class Fibonacci:
    """ interface class to link to C++ library to compute fibonacci numbers """
    def __init__(self):
        self.obj = lib.Fibonacci_new()

    def __iter__(self):
        return self

    def __next__(self):
        return lib.Fibonacci_next(self.obj)
