/* Extension interface to the C++ class
   compile with: g++ -c -fPIC fib_ext.cpp -o fib_ext.o */

#include "fib.cpp"

extern "C" {
    Fibonacci* Fibonacci_new() {
        return new Fibonacci();
    }
    int Fibonacci_next(Fibonacci* fib) {
        return fib->next();
    }
};
