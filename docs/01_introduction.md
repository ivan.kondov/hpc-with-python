Why using Python?
-----------------

* Increase scientist's productivity
* Accelerate prototyping in complex projects - make it work and then make it fast
* Reuse existing codes written in other languages

General strategies
------------------

* Detect performance critical sections using timing and profiling
* For performance irrelevant parts - program rapidly in Python 
* In performance critical sections - reuse available high performance computing (HPC) libraries 
* Add your existing HPC codes as extension modules
* You are starting a new project - start it with Python

Disclaimer
----------
* Only a *short* introduction to HPC with Python
* Assume basic knowledge of HPC and Python
* Many important aspects not covered, for example performance analysis
