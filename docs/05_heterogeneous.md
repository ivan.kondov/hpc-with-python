# Heterogeneous programming and computing with Python

There are two general approaches:
1. *Write* the performance critical code in Python or a python-base domain-specific language, such as Pyrex, and *compile* highly optimized machine code (*kernels*), and *generate* Python extension modules for them.
2. *Write* or *reuse* existing codes in high-performance oriented languages, such as C++ and Fortran, and *write* or *generate* python extension modules for them. This approach will be used in [Exercise 8](#exercise-8-linking-to-c-and-fortran).


*Disclaimer: The lists of libraries, frameworks and platforms provided here are incomplete.*

* Frameworks that compile optimized kernels
  * [Theano](https://github.com/Theano/Theano) and [Aesara](https://github.com/aesara-devs/aesara) – focus on multidimensional arrays and expressions
  * [TensorFlow](https://www.tensorflow.org/) – focus on multidimensional arrays and dataflow used in machine learning

* Just-in-time (JIT) compilers
  * [PyCUDA](https://documen.tician.de/pycuda) (NVidia GPU)
  * [PyOpenCL](https://documen.tician.de/pyopencl) (CPU/GPU)
  * [Numba](http://numba.pydata.org) (CPU/GPU)
  * [Pythran](https://pythran.readthedocs.io) (CPU)


* Linking to C, C++ and Fortran (see below)


## Linking to C, C++ and Fortran

* External codes can be linked as extension modules 
* Main approach: write or generate a wrapper

| Name | Language | Approach | Web site |
-------|----------|----------|----------|
|Python/C API | C | API (Python.h) | [docs.python.org](http://docs.python.org) |
|ctypes | C, C++ | Link to DLLs | [docs.python.org](http://docs.python.org) |
|Boost.Python | C++ | API | [www.boost.org](http://www.boost.org) |
|SWIG | C, C++ | generator | [www.swig.org](http://www.swig.org) | 
|F2PY | C, Fortran | compiler wrapper | [docs.scipy.org](http://docs.scipy.org) |
|Pyrex/Cython | C | language / compiler | [cython.org](http://cython.org) |

## Exercise 8: Linking to C++ and Fortran

### Link to C++ using `ctypes`

1. Given the class definition in `fib.cpp`
2. Write an extension interface `fib_ext.cpp`
3. Write an extension module `fib.py`

```bash
cd ctypes
make fibc++.so
python fibtest.py
```

### Link to Fortran using `f2py`

  * automatic building an extension module using module `numpy.f2py`
  * add `--fcompiler=intelem` after `-c` in makefile if you use the Intel Fortran Compiler


#### Compile and run the pure Fortran 77 program

```bash
cd f2py
make fibprog && ./fibprog
```


#### Case 1: Link to libfib1 with no explicit interface

```bash
make libfib1
python fib1.py
```


#### Case 2: Adapting the interface using a pyf-file

```bash
make fib1pyf
```

Open the file `fib1.pyf` and change:

```fortran
integer dimension(n) :: a
integer, optional, check(len(a)>=n), depend(a) :: n = len(a)
```
with 

```fortran
integer dimension(n), intent(out), depend(n) :: a
integer, intent(in) :: n
```
and save as new file `fib2.pyf`. Then build the interface and start the test program with

```bash
make libfib2
python fib2.py
```


#### Case 3: Instrumenting the Fortran source

Add to the end of the header of the fib subroutine in file `fib1.f`:

```fortran
cf2py intent(out) a
cf2py depend(n) a
cf2py intent(in) n
```

and save as new file `fib3.f`. Then build the interface and start the test program with

```bash
make libfib3
python fib3.py
```


#### Case 4: Linking to Fortran 90

* No changes necessary – just use the Fortran 90 modules and interfaces
* Result the same as in Cases 2 and 3
* Example in file `fib4.f90`

Build the interface and start the test program with

```bash
make libfib4
python fib4.py
```
