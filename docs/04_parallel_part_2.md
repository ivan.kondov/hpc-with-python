# Concurrent and parallel programming/computing with Python (Part 2)

## Task paralleism

 * The control-flow and the data-flow of the application can be modelled with a directed asyclic graph (DAG).
 * The vertices in the DAG correspond to groups of operations (tasks).
 * The edges model data dependencies from which control-flow and data-flow can be inferred.
 * Independent tasks can be executed in parallel.

 * Example: construct the letter A from images 1 and 2

   <img src="docs/images/workflow_example.png" width="400">
   <img src="docs/images/workflow_dag_example.png" width="500">

### Coarse-grained and fine-grained parallelism
* Coarse-grained
<img src="docs/images/coarse-grained.png" width="300">

* Fine-grained
<img src="docs/images/fine-grained.png" width="350">

### Workflows versus pipelines

* Workflow

  * Step 2 begins after Step 1 is ready
  * Data is passed discretely

    <img src="docs/images/workflow_vs_pipeline_1.png" width="150">
  * Python packages: [FireWorks](https://materialsproject.github.io/fireworks/), [Dask](https://dask.org/) 

* Pipeline

  * All stages run simultaneously
  * Data is passed continuously

    <img src="docs/images/workflow_vs_pipeline_2.png" width="200">
  
  * can be readily implemented with Python generators
  * Python packages: [scikit-learn](https://scikit-learn.org)




### Dask

"*[Dask](https://dask.org/) provides advanced parallelism for analytics, enabling performance at scale for the tools you love.*"

### Exercise 6: Dask distributed, Dask dataframe, Dask bag and Dask delayed

This is how to install Dask using the Intel Python distribution:

```bash
python -m pip install dask[distributed]
python -m pip install dask[bag]
```

For other distributions one might have to add
```bash
python -m pip install dask[dataframe]
python -m pip install dask[array]
```

In order to visualize task graphs we also need the `graphviz` package:

```bash
python -m pip install graphviz
```

1. The task is to find the prime numbers from the first 5000 Fibonacci numbers using 

* `dask.distributed` and comparing the `result()`, `as_completed()` and `gather()` methods;
* `dask.dataframe`;
* `dask.bag`

and make a measurement of the speedup.

2. Compute the first eight Fibonacci numbers using `dask.delayed` applied on a recursive function. Visualize the task graph with `visualize()` and compute with `compute()`.


### Exercise 7: Dask array

For an integer random vector `vec` construct the matrix `M` from the outer product of (`vec`, `vec`^T).
Then compute the trace of the matrix and the sum of all elements of the matrix `M` - `M`^T (residual) using `compute()`. Visualize the trace and the residual objects using `visualize()`.
