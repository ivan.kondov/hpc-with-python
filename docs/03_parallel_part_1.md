# Concurrent and parallel programming/computing with Python (Part 1)

## Multithreading and multiprocessing

### Python threads

* Concurrency versus parallelism
* Global Interpreter Lock (GIL)
* The multithreading package
* Preemptive multitasking

  <img src="docs/images/preemptive.png" width="350">

* Not relevant for HPC parallelization
* Relevant for processing I/O asynchronously (asyncio package)

### Python generator

* Implemented concepts:
  * Lazy evaluation / evaluation on demand 
  * Cooperative multitasking 
  * Iterator 

* In data intensive computing 
  * Save memory for IO operations 
  * Save memory for intermediately stored objects 
 
* Defined in different ways
  * Generator function: `yield` keyword 
  * Generator expression using `()` 
 
* Chains of generators - pipelines 

* Example with the `yield` keyword

```python
def data_producer():
    # do work
    return lots_of_data

def data_consumer(data):
    for element in data:
        # do work

bulky = data_producer() 
result = data_consumer(bulky) 

def smart_data_producer():
    while is_working:
        yield piece_of_data

gen = smart_data_producer()
result = data_consumer(gen)
```

* Example with generator expression

```python
# a list
data_squared = [element**2 for element in data]
data_squared_1 = data_squared[1]

# generator
data_squared_gen = (element**2 for element in data)
data_squared_0 = next(data_squared_gen)
```

### How does a generator work?

Default: The generator and consumer share the same processing element (PE).

<img src="docs/images/generator_one_pe.png" width="400">

The generator and consumer can be distributed on PEs:

<img src="docs/images/generator_two_pes.png" width="450">


* Benefit: Task parallelism (Multiple Processes -- Single Data, MPSD)
* Drawback: possible load imbalance

### Exercise 2: Generate number sequences

* Calculate the first eight Fibonacci numbers 
* Calculate the primes in a given range of numbers 
* Find the first ten prime numbers from the Fibonacci sequence

```bash
cd generator 
# traditional method 
python fib1.py 
 
# infinite generator function 
python fib2.py 
 
# infinite iterator class 
python fib3.py 
 
# primes 
python primes.py 
 
# Fibonacci primes 
python fibprimes.py
```

In the last example the `isprime()` function from the `sympy` package is used. What happens when you switch back to `isprime()` from `primes.py`?

### Message Passing Interface via the `mpi4py` package

* Message passing: a paradigm for parallel computing
* [Message Passing Interface (MPI)](https://www.mpi-forum.org/) – the standard
* Allows distributed computing on many cluster nodes
* The [mpi4py](https://mpi4py.readthedocs.io) package – a Python interface to MPI

* Message types
  * Point-to-point communication
    - Blocking send/recv
    - Non-blocking isend/irecv
  * Collective communication: broadcast, scatter, gather, reduce, ...

* The item count and MPI datatype discovered automatically for
  * `lower`-case methods: generic Python objects - more flexibility due to serialization
  * `Upper`-case methods: numpy arrays with fixed datatypes and size - more communication performance


### Exercise 3: Installation and setup of the `mpi4py` package

The mpi4py package is included in the Intel Python distribution. Therefore, it is not needed to install it. With other distributions mpi4py can be installed with the following steps

```bash
module load compiler/gnu
module load mpi/openmpi
. venv-python-3.6.8/bin/activate # if not activated
python -m pip install mpi4py
```

The mpi4py installation should be tested before going on with the examples.

```bash
mpiexec -n 3 python -m mpi4py.bench helloworld 
mpiexec -n 5 python -m mpi4py.bench ringtest -n 1024 -l 1000 
```

### Note on mpi4py runtime environment

* Usually the application is launched with 
 
```bash
mpiexec -n <nprocs> python pyfile.py
```

An unhandled exception (e.g. division by zero) may cause a deadlock. Instead, there is a mechanism catching and handling such exceptions:

```bash
mpiexec -n numprocs python -m mpi4py pyfile [arg] ... 
mpiexec -n numprocs python -m mpi4py -m mod [arg] ... 
mpiexec -n numprocs python -m mpi4py -c cmd [arg] ... 
mpiexec -n numprocs python -m mpi4py - [arg] ... 
```


### Exercise 4: Launching mpi4py programs

The “standard” way:

```bash
cd mpi4py
mpiexec -n 2 python deadlock.py
```

What happens? Why is there no deadlock if the exception occurs in rank 1?

Now let us launch the script in another way (please make sure the exception is in rank 0):

```bash
mpiexec -n 2 python -m mpi4py deadlock.py
```

Why is there no deadlock?


### Data parallelism

* SPMD (Single Process Multiple Data) and MPMD 
* Fine-grained vs. coarse grained parallelism

| package[.module][.class] | Methods | Parallel | Many nodes|
|--------------------------|---------|----------|-----------|
| built-in/functools       | map, filter, reduce | No | No  |
| itertools                | starmap | No | No  |
| multiprocessing.Pool     | map, map_async, imap, imap_unordered, starmap, starmap_async, apply, apply_async | Yes | No | 
| concurrent.futures.ProcessPoolExecutor | map, submit, shutdown | Yes | No | 
| mpi4py.futures.MPICommExecutor | map, starmap, submit, bootup, shutdown | Yes | Yes |
| mpi4py.futures.MPIPoolExecutor | map, starmap, submit, bootup, shutdown | Yes | Yes |
| dask.distributed.Client | map, apply, submit, gather | Yes | Yes |


### Parallelizing the map method

* Len(Data) == N(P)

  <img src="docs/images/map_eq_np_cz1.png" width="400">

* Len(Data) > N(P)

  <img src="docs/images/map_gt_np_cz1.png" width="600">

* Len(Data) > N(P) and chunk size == 2

  <img src="docs/images/map_gt_np_cz2.png" width="600">

### Exercise 5: Parallelize the map method

* Find the primes from the first 5000 Fibonacci numbers 
* Measure the run time with different parallelization packages, number of processes and chunk sizes

```bash
cd spmd 
 
# serial 
python map.py 
 
# multiprocessing package 
python multiprocessing_pool.py 
 
# concurrent.futures package 
python process_pool_executor.py 
 
# mpi4py package, static launch, 1 master, 4 workers 
mpiexec -n 5 python -m mpi4py mpi_comm_executor.py 
 
# mpi4py package, dynamic launch (spawn), 1 master, up to 4 workers 
mpiexec -n 1 -usize 5 python -m mpi4py mpi_pool_executor.py
```
