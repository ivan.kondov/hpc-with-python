# Python distributions and virtual environments

## [CPython](https://www.python.org/) (the standard Python distribution)

* Versions available on HoreKa
  - 3.6.8 system-wide
  - 3.6.8 as part of Jupyter modules `devel/jupyter_base` and `devel/jupyter_ml` 

## [Anaconda Python](https://www.anaconda.com/)

* Focusing on scientific and engineering applications

## [Intel Python](https://www.intel.com/content/www/us/en/developer/tools/oneapi/distribution-for-python.html)

* Based on Anaconda Python
* Leverages Intel MKL, Intel TBB and Intel DAAL for high performance
* Versions available on HoreKa:
  - 3.6.9 in module `devel/python/3.6_intel`

## Python Versions

* Recommended Python versions: 3.x
* The most recent version: 3.10
* End of life of Python 2 in 2020 (not recommended)

## Virtual environment (`venv`)

* Isolated custom installation of python packages
* Switching with commands `activate` and `deactivate`
* For working with multiple projects with conflicting requirements


# The SciPy collection

* [NumPy](https://numpy.org/) – numerical computation with arrays
* [SymPy](https://www.sympy.org) – symbolic math computation
* [SciPy](https://scipy.org/) library – a library for scientific computing
* [Pandas](https://pandas.pydata.org/) – data analysis
* [Matplotlib](https://matplotlib.org/)  – data visualization
* [IPython](https://ipython.org/) – interactive Python

# Exercise 1: Installation and setup of the scipy collection on HoreKa

## Based on Intel Python (recommended)

```bash
# prepare the global environment
module purge
module load devel/python/3.6_intel

# create a virtual environment
python -m venv --without-pip --system-site-packages venv-python-3.6_intel

# activate the virtual environment
. venv-python-3.6_intel/bin/activate

# install pip
pip install -t venv-python-3.6_intel/lib/python3.6/site-packages pip

# deactivate the virtual environment
deactivate

# activate again
. venv-python-3.6_intel/bin/activate

# only sympy and ipython not available in the distribution
python -m pip install sympy
python -m pip install ipython
```


## Based on CPython (version 3.6.8)

```bash
# cleanup all loaded modules
module purge

# create a virtual environment
python -m venv venv-python-3.6.8

# activate the virtual environment
. venv-python-3.6.8/bin/activate

# upgrade pip
python -m pip install --upgrade pip

# intel-optimized intel-numpy and intel-scipy
python -m pip install intel-scipy
# pandas package not compatible with available intel-numpy
python -m pip install sympy
python -m pip install matplotlib
python -m pip install ipython
```
