""" using the fortran 90 interface """
from libfib4 import fib

print(fib.__doc__)
print('effect the same as with instrumentation in fib3.py:')
print(fib(8))
