""" using instrumentation in file fib3.f """
from libfib3 import fib

print(fib.__doc__)
print('effect the same as with fib2.py:')
print(fib(8))
