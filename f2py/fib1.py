""" without supplying an interface """
import numpy as np
from libfib1 import fib

print(fib.__doc__)
a = np.empty(8, dtype=np.int32)

print('\nskipping optional n:')
fib(a)
print(a)

print('\nwith optional n:')
fib(a, 8)
print(a)

print('\ninvalid input type leads to unexpected results')
a = np.ones(8, dtype=np.float)
fib(a)
print(a)

print('\nincorrect value of optional n:')
fib(a, 10)
print(a)
