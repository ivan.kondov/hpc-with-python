# High Performance Computing with Python

This tutorial is given within the [NHR training program](https://www.nhr.kit.edu/dokumentation_veranstaltungen.php) and includes an introduction to High Performance Computing (HPC) using Python.

## Prerequisites

To run the examples in this tutorial you will need a Linux computer on that the following software is installed:

* A Python 3 distribution
* C, C++ and Fortran compilers
* An MPI distribution such as Open MPI
* A processor with two or more CPU cores
* A text editor

On [HoreKa](https://www.scc.kit.edu/en/services/horeka) (the HPC system that we are targeting) all this is already installed and available. The tutorial itself can be installed locally via this command

```bash
git clone https://gitlab.kit.edu/ivan.kondov/hpc-with-python.git
```


## Contents
* [Introduction](docs/01_introduction.md)
* [Python distributions, virtual environments the SciPy collection](docs/02_environment_scipy.md)
  - [Exercise 1](docs/02_environment_scipy.md#exercise-1-installation-and-setup-of-the-scipy-collection-on-horeka): Installation and setup of the scipy collection on HoreKa
* [Concurrent and parallel programming/computing with Python (Part 1)](docs/03_parallel_part_1.md)
  - Multithreading and multiprocessing
  - [Exercise 2](docs/03_parallel_part_1.md#exercise-2-generate-number-sequences): Generators
  - Message Passing Interface
    - [Exercise 3](docs/03_parallel_part_1.md#exercise-3-installation-and-setup-of-the-mpi4py-package): Installation and setup of the `mpi4py` package on HoreKa
    - [Exercise 4](docs/03_parallel_part_1.md#exercise-4-launching-mpi4py-programs): Launching `mpi4py` programs
  - Data parallelism
    - [Exercise 5](docs/03_parallel_part_1.md#exercise-5-parallelize-the-map-method): Parallelizing the map method
* [Concurrent and parallel programming/computing with Python (Part 2)](docs/04_parallel_part_2.md)
  - Task paralleism
    - Fine-grained applications and coarse-grained applications, workflows
    - [Exercise 6](docs/04_parallel_part_2.md#exercise-6-dask-distributed-dask-delayed-dask-dataframe-and-dask-bag): Dask distributed, Dask delayed, Dask dataframe and Dask bag
    - [Exercise 7](docs/04_parallel_part_2.md#exercise-7-dask-array): Dask array
* [Heterogeneous computing with Python](docs/05_heterogeneous.md)
  - Compiling optimized kernels
  - Just-in-time compilers
  - Linking to C, C++ and Fortran
  - [Exercise 8](docs/05_heterogeneous.md#exercise-8-linking-to-c-and-fortran): Linking to C++ and Fortran
