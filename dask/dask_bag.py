""" use the dask.bag to parallelize filter function """
import itertools
import dask.bag as bg
from dask.distributed import Client
from sympy import isprime
from fib2 import fibonacci
from timing import timeit


@timeit
def fibonacci_primes():
    """ find the primes from the first 5000 Fibonacci numbers """
    procs = 4
    fib_chunk_size = 5000
    chunk_size_per_proc = 256

    fchunk = list(itertools.islice(fibonacci(), fib_chunk_size))
    bag = bg.from_sequence(fchunk, npartitions=chunk_size_per_proc)
    bag_filter = bag.filter(isprime)
    '''
    bag = bg.from_sequence(fchunk, npartitions=4)
    bag_filter = bag.filter(isprime)
    bag_filter.visualize(filename='dask_bag.png')
    '''
    with Client(n_workers=procs):
        primes = bag_filter.compute()

    print('procs:', procs)
    print('number of fibonacci numbers:', fib_chunk_size)
    print('chunk size per proc:', chunk_size_per_proc)
    print('number of primes:', len(primes))


if __name__ == '__main__':
    fibonacci_primes()
