""" parallelize linear algebra with dask.array """
import dask.array as da
from dask.distributed import Client
from timing import timeit


@timeit
def get_residual(darr, nprocs):
    """ compute M - M^t """
    darr.visualize(filename='residual.png')
    with Client(n_workers=nprocs):
        res = darr.compute()
    return res


@timeit
def get_trace(darr, nprocs):
    """ compute the trace of a matrix """
    darr.visualize(filename='trace.png')
    with Client(n_workers=nprocs):
        res = darr.compute()
    return res


if __name__ == '__main__':
    procs = 4
    chunksize = 2
    arr = da.random.random((5000), chunks=(1000,))
    mat = da.outer(arr, arr.T)
    residual = (mat-mat.T).sum(axis=0).sum(axis=0)
    trace = mat.trace()
    print('procs:', procs)
    print('chunk size', chunksize)
    print(get_residual(residual, procs))
    print(get_trace(trace, procs))
