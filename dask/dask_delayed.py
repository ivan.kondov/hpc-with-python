""" use dask.delayed to compute the first eight fibonacci numbers """
from dask import delayed


def fibonacci_one(numb):
    """ return the nth Fibonacci number """
    if numb < 2:
        ret = numb
    else:
        ret = delayed(fibonacci_one(numb-1)) + delayed(fibonacci_one(numb-2))
    return ret


def fibonacci_all(numb):
    """ return the first n Fibonacci numbers """
    lst = [0, 1]
    for _ in range(2, numb):
        lst.append(delayed(lst[-1]+lst[-2]))
    return delayed(lst)


if __name__ == '__main__':
    fib = fibonacci_one(8)
    fib.visualize(filename='fibonacci_one.png')

    fib = delayed(map(fibonacci_one, range(8)))
    print(fib.compute())

    fib = fibonacci_all(8)
    fib.visualize(filename='fibonacci_all.png')
    print(fib.compute())
