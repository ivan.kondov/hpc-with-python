""" use the dask.distributed module to parallelize the map function """
import itertools
from sympy import isprime
from dask.distributed import Client
# from dask.distributed import as_completed
from fib2 import fibonacci
from timing import timeit


@timeit
def fibonacci_primes():
    """ find the primes from the first 5000 Fibonacci numbers """
    procs = 4
    fib_chunk_size = 5000
    chunk_size_per_proc = 256

    fchunk = list(itertools.islice(fibonacci(), fib_chunk_size))
    with Client(n_workers=procs) as client:
        futures = client.map(isprime, fchunk, batch_size=chunk_size_per_proc)
        prmask = map(lambda x: x.result(), futures)
        # prmask = [r for f, r in as_completed(futures, with_results=True)]
        # prmask = client.gather(futures)
        primes = list(itertools.compress(fchunk, prmask))

    print('procs:', procs)
    print('number of fibonacci numbers:', fib_chunk_size)
    print('chunk size per proc:', chunk_size_per_proc)
    print('number of primes:', len(primes))


if __name__ == '__main__':
    fibonacci_primes()
