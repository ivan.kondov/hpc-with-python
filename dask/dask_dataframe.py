""" use the dask.dataframe to parallelize finding prime fibonacci numbers """
import itertools
import pandas as pd
import dask.dataframe as dd
from dask.distributed import Client
from sympy import isprime
from fib2 import fibonacci
from timing import timeit


@timeit
def pandas_dataframe():
    """ Using Pandas dataframe """
    pdf = pd.DataFrame(fchunk, columns=['fibonacci'])
    pdf['prime'] = pdf['fibonacci'].apply(isprime)
    print(pdf.groupby('prime').count())


@timeit
def dask_dataframe():
    """ Using Dask dataframe """
    pdf = pd.DataFrame(fchunk, columns=['fibonacci'])
    ddf = dd.from_pandas(pdf, npartitions=chunk_size_per_proc)
    ddf['prime'] = ddf['fibonacci'].apply(isprime, meta=('prime', 'bool'))
    with Client(n_workers=procs):
        ddf_result = ddf.compute()
    print(ddf_result.groupby('prime').count())


if __name__ == '__main__':
    procs = 4
    fib_chunk_size = 5000
    chunk_size_per_proc = 256
    fchunk = list(itertools.islice(fibonacci(), fib_chunk_size))
    print('procs:', procs)
    print('number of fibonacci numbers:', fib_chunk_size)
    print('chunk size per proc:', chunk_size_per_proc)
    pandas_dataframe()
    dask_dataframe()
