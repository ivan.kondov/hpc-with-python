""" find the first ten prime numbers from the Fibonacci sequence """
import itertools
# from primes import isprime
from sympy import isprime
from fib2 import fibonacci

primes = (n for n in fibonacci() if isprime(n))
slc = itertools.islice(primes, 10)
print(list(slc))
