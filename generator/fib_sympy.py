""" fibonacci numbers from the sympy package """
from sympy import fibonacci

print([fibonacci(i) for i in range(8)])
