""" Prime numbers examples """


def isprime(numb):
    """ test whether a number is a prime number """
    return not (numb < 2 or any(numb % i == 0 for i in range(2, numb)))


if __name__ == "__main__":
    range_of_interest = range(5, 1000)

    # print the primes using a list comprehension
    primes = [n for n in range_of_interest if isprime(n)]
    print('from a list comprehension', primes)

    # the same with a generator expression
    primes = (n for n in range_of_interest if isprime(n))
    print('from a generator expression', list(primes))
