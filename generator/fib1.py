""" calculate and return a list of the first n Fibonacci numbers """


def fibonacci(number):
    """ compute the first n=number fibonacci numbers """
    fib = [0, 1]
    while len(fib) < number:
        fib.append(fib[-1]+fib[-2])
    return fib


if __name__ == '__main__':
    print(fibonacci(8))
