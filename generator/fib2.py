""" Fibonacci numbers as infinite generator function """


def fibonacci():
    """ generator function yielding the next fibonacci number """
    old, new = 0, 1
    yield old
    yield new
    while True:
        old, new = new, old + new
        yield new


if __name__ == '__main__':

    # initialize the (infinite) generator
    gen1 = fibonacci()
    chunk_size = 8

    # 1. approach: a generator function
    def firstn(gen, number):
        """ a generic generator slice function """
        for _ in range(number):
            yield next(gen)

    gen2 = firstn(gen1, chunk_size)
    print('generator function', list(gen2))

    # 2. approach: a generator expression
    gen3 = (next(gen1) for i in range(chunk_size))
    print('generator expression', list(gen3))

    # 3. approach: generate a list directly
    print('list', [next(gen1) for i in range(chunk_size)])

    # 4. approach: use islice
    import itertools
    slc = itertools.islice(gen1, chunk_size)
    print('itertools.islice', list(slc))
