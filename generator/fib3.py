""" calculate the Fibonacci numbers using an iterator class """
import itertools


class FibonacciNumbers:
    """ iterator class to compute the fibonacci numbers """
    first, second = 0, 1

    def __iter__(self):
        return self

    def __next__(self):
        ret = self.first
        self.first, self.second = self.second, self.first + self.second
        return ret


if __name__ == '__main__':
    fib = FibonacciNumbers()

    # consumer: list from iterator slice
    print('iterator slice:', list(itertools.islice(fib, 8)))

    # consumer: for loop over iterator slice
    for numb in itertools.islice(fib, 8):
        print(numb)
